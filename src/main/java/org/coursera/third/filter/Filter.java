package org.coursera.third.filter;

public interface Filter {
    boolean satisfies(String id);
}
