package org.coursera.third.filter.impl;

import org.coursera.third.filter.Filter;

public class TrueFilter implements Filter {
    @Override
    public boolean satisfies(String id) {
        return true;
    }
    
}
