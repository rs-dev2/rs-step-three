package org.coursera.third.filter.impl;

import org.coursera.third.db.MovieDatabase;
import org.coursera.third.filter.Filter;

public class GenreFilter implements Filter {
    private String genre;
    
    public GenreFilter(String genre) {
        this.genre = genre;
    }
    
    @Override
    public boolean satisfies(String id) {
        return MovieDatabase.getGenres(id).contains(genre);
    }
}
