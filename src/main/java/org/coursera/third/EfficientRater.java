package org.coursera.third;

import org.coursera.entities.Rating;
import org.coursera.rater.Rater;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class EfficientRater implements Rater {
    private final Map<String, Rating> ratings;
    private final String id;

    public EfficientRater(String id) {
        this.id = id;
        ratings = new HashMap<>();
    }

    public void addRating(String item, double rating) {
        ratings.put(item, new Rating(item, rating));
    }

    public boolean hasRating(String item) {
        return ratings.containsKey(item);
    }

    public String getId() {
        return id;
    }

    public double getRating(String item) {
        if (ratings.containsKey(item)) {
            return ratings.get(item).getValue();
        }
        return -1;
    }

    public int numRatings() {
        return ratings.size();
    }

    public List<String> getItemsRated() {
        return new ArrayList<>(ratings.keySet());
    }
}
